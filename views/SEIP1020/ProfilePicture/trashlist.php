<?php
session_start();

include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP1020\ProfilePicture\ImageUploader;
use App\Bitm\SEIP1020\Utility\Utility;
use App\Bitm\SEIP1020\Message\Message;

$profile_picture= new ImageUploader();
$allinfo=$profile_picture->trashed();
//Utility::d($allBook);



?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

</head>
<body>

<div class="container">
    <h2>All Info List</h2>
    <a href="index.php" class="btn btn-primary" role="button">View All List</a>
    <a href="recoverSelected.php" class="btn btn-primary" role="button">Recover Selected</a>
    <a href="deleteSelected.php" class="btn btn-primary" role="button">Delete Selected</a>
    <div id="message">
        <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
            echo Message::message();
        }
        ?>
    </div>
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <td>Check Item</td>
                <th>#</th>
                <th>ID</th>
                <th>Name</th>
                <th>Image</th>
                <th>Action</th>

            </tr>
            </thead>
            <tbody>
            <tr>
                <?php
                $sl=0;
                foreach($allinfo as $info){
                $sl++; ?>
                <td><input type="checkbox" name="mark[]" value="<?php echo $info->id ?> ></td>
                <td><?php echo $sl?></td>
                <td><?php echo $info-> id?></td>
                <td><?php echo $info->name?></td>
                <td><img src="../../../Resources/Images/<?php echo $info->images ?>" alt="image" height="100px" width="100px" class="img-responsive"> </td>
                <td>
                    <a href="recover.php?id=<?php echo $info-> id ?>" class="btn btn-primary" role="button">Recover</a>

                    <a href="delete.php?id=<?php echo $info->id?>" class="btn btn-danger" role="button" id="delete"  Onclick="return ConfirmDelete()">Delete</a>

                </td>

            </tr>
            <?php }?>


            </tbody>
        </table>
    </div>
</div>
<script>
    $('#message').show().delay(2000).fadeOut();


    //        $(document).ready(function(){
    //            $("#delete").click(function(){
    //                if (!confirm("Do you want to delete")){
    //                    return false;
    //                }
    //            });
    //        });
    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }

</script>

</body>
</html>
